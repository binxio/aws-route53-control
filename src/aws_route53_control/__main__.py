import click
from aws_route53_control.zone_to_cfn import command


@click.group
def main():
    """
    CLI for managing AWS Route53 managed zones
    """
    pass


main.add_command(command)

if __name__ == "__main__":
    main()
