import click
from blockstack_zones import parse_zone_file
import json
import logging
import re
import sys
import os
from pathlib import Path
from ruamel.yaml import YAML, CommentedMap
from aws_route53_control.logger import logging
from easyzone import easyzone
from dns.exception import SyntaxError


def camel_case(s):
    s = re.sub(r"(_|-|\.)+", " ", s).title().replace(" ", "")
    return "".join([s[0].upper(), s[1:]])


def generate_unique_logical_resource_id(prefix: str, resources: dict) -> str:
    same_prefix = set(filter(lambda n: n.startswith(prefix), resources.keys()))
    if not same_prefix:
        return prefix

    count = 1
    while f"{prefix}{count}" in same_prefix:
        count = count + 1
    return f"{prefix}{count}"


def convert_to_cloudformation(zone: easyzone.Zone) -> dict:
    ttl = zone.root.ttl
    domain_name = zone.domain

    result = CommentedMap()
    result["AWSTemplateFormatVersion"] = "2010-09-09"
    resources = CommentedMap()
    resources["HostedZone"] = CommentedMap(
        {"Type": "AWS::Route53::HostedZone", "Properties": {"Name": domain_name}}
    )
    result["Resources"] = resources

    for key, name in zone.names.items():
        for rectype in ["A", "NS", "MX", "AAAA", "TXT", "CNAME", "PTR"]:
            records = name.records(rectype)
            if not records:
                continue

            if rectype == "NS" and key == zone.domain:
                logging.warning("ignoring NS records for origin %s", key)
                continue

            logical_name = generate_unique_logical_resource_id(
                re.sub(
                    r"[^0-9a-zA-Z]",
                    "",
                    camel_case(
                        re.sub(
                            r"^\*",
                            "wildcard",
                            key.removesuffix("." + zone.domain)
                            if key != "@"
                            else "Origin",
                        )
                    ),
                )
                + records.type
                + "Record",
                resources,
            )
            resource_records = records.items
            if rectype == "MX":
                resource_records = list(map(lambda r : f"{r[0]} {r[1]}", records.items))

            resources[logical_name] = CommentedMap(
                {
                    "Type": "AWS::Route53::RecordSet",
                    "Properties": {
                        "Name": key,
                        "Type": records.type,
                        "ResourceRecords": resource_records,
                        "TTL": name.ttl,
                        "HostedZoneId": {"Ref": "HostedZone"},
                    },
                }
            )

    return result


def target_file(src: Path, dst: Path) -> Path:
    if dst.is_file():
        return dst

    if src.suffix == ".zone":
        return dst.joinpath(src.name).with_suffix(".yaml")

    return dst.joinpath(src.name + ".yaml")


@click.command(name="zone-to-cfn")
@click.argument("src", nargs=-1, type=click.Path())
@click.argument("dst", nargs=1, type=click.Path())
def command(src, dst):
    """
    convert zonefiles into cloudformation templates
    """
    if not src:
        raise click.UsageError("no source files were specified")

    inputs = []
    for filename in map(lambda s: Path(s), src):
        if filename.is_dir():
            inputs.extend(
                [f for f in filename.iterdir() if f.is_file() and f.suffix == ".zone"]
            )
        else:
            inputs.append(filename)

    if len(inputs) == 0:
        click.UsageError("no zone files were found")

    dst = Path(dst)
    if len(inputs) > 1:
        if dst.exists() and not dst.is_dir():
            raise click.UsageError(f"{dst} is not a directory")
        if not dst.exists():
            dst.mkdir(exist_ok=True)

    outputs = list(map(lambda d: target_file(d, dst), inputs))

    for i, input in enumerate(map(lambda s: Path(s), inputs)):
        with input.open("r") as file:
            content = file.read()
            found = re.search(
                r"\$ORIGIN\s+(?P<domain_name>.*)\s*",
                content,
                re.MULTILINE | re.IGNORECASE,
            )
            if found:
                domain_name = found.group("domain_name")
            else:
                domain_name = input.name.removesuffix(".zone")
                logging.warning("could not find origin from zone file %s, using %s", input, domain_name)

            try:
                logging.info("reading zonefile %s", input.as_posix())
                zone = easyzone.zone_from_file(domain_name, input.as_posix())
            except SyntaxError as error:
                logging.error(error)
                exit(1)

        with outputs[i].open("w") as file:
            YAML().dump(convert_to_cloudformation(zone), stream=file)


if __name__ == "__main__":
    command()
